//
//  StateKit.h
//  StateKit
//
//  Created by Christopher Head on 7/25/18.
//  Copyright © 2018 chead. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for StateKit.
FOUNDATION_EXPORT double StateKitVersionNumber;

//! Project version string for StateKit.
FOUNDATION_EXPORT const unsigned char StateKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StateKit/PublicHeader.h>


