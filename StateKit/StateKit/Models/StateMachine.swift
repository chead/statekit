//
//  StateKit.swift
//  StateKit
//
//  Created by Christopher Head on 7/25/18.
//  Copyright © 2018 chead. All rights reserved.
//

import Foundation

public class StateMachine<State: Hashable, Event: Hashable>: StateMaintaining {
    public enum Error: Swift.Error {
        case invalidDestination(for: Event)
    }

    public private(set) var state: State
    public let routes: Set<Route<Transition<State>, Event>>

    public init(state: State, routes: Set<Route<Transition<State>, Event>>) {
        self.state = state
        self.routes = routes
    }

    public func transition(for event: Event) throws {
        guard
            let transition = (self.routes.filter({ $0.transition.source == state }).first { return $0.event == event }?.transition)
            else { throw Error.invalidDestination(for: event) }

        transition.completion()

        self.state = transition.destination
    }
}
