//
//  Transition.swift
//  StateKit
//
//  Created by Christopher Head on 7/27/18.
//  Copyright © 2018 chead. All rights reserved.
//

import Foundation

public struct Transition<State: Hashable>: TransitionRepresenting, Hashable {
    public let source:      State
    public let destination: State
    public let completion:  () -> Void

    public init(source: State, destination: State, completion: @escaping () -> Void) {
        self.source = source
        self.destination = destination
        self.completion = completion
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.source.hashValue)
        hasher.combine(self.destination.hashValue)
    }

    public static func == (lhs: Transition<State>, rhs: Transition<State>) -> Bool {
        return lhs.source.hashValue == rhs.source.hashValue
    }
}
