//
//  Route.swift
//  StateKit
//
//  Created by Christopher Head on 7/27/18.
//  Copyright © 2018 chead. All rights reserved.
//

import Foundation

public struct Route<Transition: TransitionRepresenting & Hashable, Event: Hashable>: RouteRepresenting, Hashable {
    public let transition: Transition
    public let event:      Event

    public init(transition: Transition, event: Event) {
        self.transition = transition
        self.event = event
    }
}
