//
//  RouteRepresenting.swift
//  StateKit
//
//  Created by Christopher Head on 7/27/18.
//  Copyright © 2018 chead. All rights reserved.
//

import Foundation

public protocol RouteRepresenting {
    associatedtype Transition: TransitionRepresenting
    associatedtype Event

    var transition: Transition { get }
    var event:      Event { get }
}
