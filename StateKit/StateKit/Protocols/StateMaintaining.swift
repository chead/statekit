//
//  Protocols.swift
//  StateKit
//
//  Created by Christopher Head on 7/27/18.
//  Copyright © 2018 chead. All rights reserved.
//

import Foundation

public protocol StateMaintaining {
    associatedtype State

    var state: State { get }
}
