//
//  TransitionRepresenting.swift
//  StateKit
//
//  Created by Christopher Head on 7/27/18.
//  Copyright © 2018 chead. All rights reserved.
//

import Foundation

public protocol TransitionRepresenting {
    associatedtype State

    var source:         State { get }
    var destination:    State { get }
    var completion:     () -> Void { get }
}
