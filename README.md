# StateKit

A simple state machine implementation using reference types and generics, created to ease state-keeping when developing complex View Controllers.