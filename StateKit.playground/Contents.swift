import Foundation
import StateKit

enum State {
    case foo
    case bar
    case baz
}

enum Event: String {
    case foo
    case bar
    case baz
}

let fooToBar = Transition(source:      State.foo,
                          destination: State.bar,
                          completion: { print("Transitioned from Foo to Bar") })

let barToBaz = Transition(source:      State.bar,
                          destination: State.baz,
                          completion: { print("Transitioned from Bar to Baz") })

let bazToFoo = Transition(source:      State.baz,
                          destination: State.foo,
                          completion: { print("Transitioned from Baz to Foo") })

let fooToBarForBar = Route(transition: fooToBar,
                           event:      Event.bar)

let barToBazForBaz = Route(transition: barToBaz,
                           event:      Event.baz)

let bazToFooForFoo = Route(transition: bazToFoo,
                           event:      Event.foo)

let stateMachine = StateMachine(state: State.foo, routes: [fooToBarForBar,
                                                           barToBazForBaz,
                                                           bazToFooForFoo])

try! stateMachine.transition(for: .bar)
try! stateMachine.transition(for: .baz)
try! stateMachine.transition(for: .foo)

do { try stateMachine.transition(for: .baz) }
catch let error as StateMachine<State, Event>.Error {
    switch error {
    case .invalidDestination(let event):
        print("Invalid transition to: \(event.rawValue)")
    }
}
